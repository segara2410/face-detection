# Face Recogniion

## Install Dependencies
- Install **Dependencies**.
  ``` sh
  ~$ pip install --user cmake
  ~$ pip install --user face_recognition
  ~$ pip install --user dlib
  ~$ pip install --user opencv-python
  ```

## Run The Face Recognition
- Run the program using
  ```
  python src/facerecognition.py
  ```